# docker-python-pcl

## add pypcap module [2018.4.8]

based on ubuntu:14.04
use pcl 1.7 official binary deb packages
include python-pcl

only clone the python-pcl not installed cause it timeout on dockerhub.

visualization module is commented on ubuntu/OSX platform, need manully uncomment it to compile.
uncomment line 546-548, for pcl 1.7 vtk include and libraries.
uncomment line 610 to add visualization module.
check my answer on https://github.com/strawlab/python-pcl/issues/127#issuecomment-379522531